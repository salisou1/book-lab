<?php

namespace ContainerMZHJLOr;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder2d92c = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer258b8 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5de07 = [
        
    ];

    public function getConnection()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getConnection', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getMetadataFactory', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getExpressionBuilder', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'beginTransaction', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getCache', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getCache();
    }

    public function transactional($func)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'transactional', array('func' => $func), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->transactional($func);
    }

    public function commit()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'commit', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->commit();
    }

    public function rollback()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'rollback', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getClassMetadata', array('className' => $className), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'createQuery', array('dql' => $dql), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'createNamedQuery', array('name' => $name), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'createQueryBuilder', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'flush', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'clear', array('entityName' => $entityName), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->clear($entityName);
    }

    public function close()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'close', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->close();
    }

    public function persist($entity)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'persist', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'remove', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'refresh', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'detach', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'merge', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getRepository', array('entityName' => $entityName), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'contains', array('entity' => $entity), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getEventManager', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getConfiguration', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'isOpen', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getUnitOfWork', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getProxyFactory', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'initializeObject', array('obj' => $obj), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'getFilters', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'isFiltersStateClean', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'hasFilters', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return $this->valueHolder2d92c->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer258b8 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder2d92c) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder2d92c = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder2d92c->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, '__get', ['name' => $name], $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        if (isset(self::$publicProperties5de07[$name])) {
            return $this->valueHolder2d92c->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2d92c;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2d92c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2d92c;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2d92c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, '__isset', array('name' => $name), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2d92c;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder2d92c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, '__unset', array('name' => $name), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2d92c;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder2d92c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, '__clone', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        $this->valueHolder2d92c = clone $this->valueHolder2d92c;
    }

    public function __sleep()
    {
        $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, '__sleep', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;

        return array('valueHolder2d92c');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer258b8 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer258b8;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer258b8 && ($this->initializer258b8->__invoke($valueHolder2d92c, $this, 'initializeProxy', array(), $this->initializer258b8) || 1) && $this->valueHolder2d92c = $valueHolder2d92c;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder2d92c;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder2d92c;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
